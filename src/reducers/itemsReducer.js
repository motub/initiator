const itemsReducer = (state, action) => {
  let items = [...state];
  switch (action.type) {
    case "addItem":
      items = items.filter(i => {
        return i.itemServiceId !== action.data.itemServiceId;
      });
      items.push(action.data);
      return items;
    case "removeItem":
      return items.filter(i => {
        return i.itemServiceId !== action.data.itemServiceId;
      });
    default:
      console.log("Unknown action " + action.type);
      throw new Error();
  }
};

export { itemsReducer };
