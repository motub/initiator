const initialState = {
  customer: {
    firstname: "",
    lastname: "",
    email: "",
    phone: ""
  },
  address: {
    line1: "",
    line2: "",
    suburb: "",
    state: "",
    postcode: ""
  },
  specialNotes: "",
  collectionDate: new Date(),
  deliveryDate: new Date()
};

const jobReducer = (state, action) => {
  let job = { ...state };
  switch (action.type) {
    case "collectDate":
      job.collectionDate = action.data;
      return job;
    case "deliveryDate":
      job.deliveryDate = action.data;
      return job;
    case "firstName":
      job.customer.firstname = action.data;
      return job;
    case "lastName":
      job.customer.lastname = action.data;
      return job;
    case "email":
      job.customer.email = action.data;
      return job;
    case "phone":
      job.customer.phone = action.data;
      return job;
    case "addressLine1":
      job.address.line1 = action.data;
      return job;
    case "addressLine2":
      job.address.line2 = action.data;
      return job;
    case "addressSuburb":
      job.address.suburb = action.data;
      return job;
    case "addressState":
      job.address.state = action.data;
      return job;
    case "addressPostcode":
      job.address.postcode = action.data;
      return job;
    case "specialNotes":
      job.specialNotes = action.data;
      return job;
    default:
      console.log("Unknown action " + action.type);
      throw new Error();
  }
};

export { jobReducer, initialState };
