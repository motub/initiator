import React, { useState, useRef, useEffect } from "react";

function Product({ product }) {
  const [paidFor, setPaidFor] = useState(false);
  const [error, setError] = useState(null);
  const paypalRef = useRef();

  useEffect(() => {
    window.paypal
      .Buttons({
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: product.description,
                amount: {
                  currency_code: "AUD",
                  value: product.price
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          setPaidFor(true);
          console.log(order);
        },
        onError: err => {
          setError(err);
          console.error(err);
        }
      })
      .render(paypalRef.current);
  }, [product.description, product.price]);

  if (paidFor) {
    return (
      <div>
        <h1>Your paymnent for {product.name}, was successful</h1>
      </div>
    );
  }

  return (
    <div>
      {error && <div>An error occurred: {error.message}</div>}
      <h1>
        Make payment for {product.description} at ${product.price}
      </h1>
      <div ref={paypalRef} />
    </div>
  );
}

function Paypal() {
  const product = {
    price: 7.77,
    name: "Washing Services",
    description: "7kg package"
  };

  return (
    <div className="Paypal">
      <Product product={product} />
    </div>
  );
}

export default Paypal;
