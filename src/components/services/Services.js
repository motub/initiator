import React, { useState, useEffect, useContext } from "react";
import { API, graphqlOperation } from "aws-amplify";
import { withTranslation } from "react-i18next";
import { listServices } from "../../graphql/queries";
import StateContext from "../../contexts/stateContext";

const Services = ({ t }) => {
  const [quantity, setQuantity] = useState("1");
  const [service, setService] = useState("");
  const [services, setServices] = useState([]);
  const [servicesMap, setServicesMap] = useState([]);
  const { items, itemsDispatch } = useContext(StateContext);

  useEffect(_ => {
    API.graphql(graphqlOperation(listServices, {})).then(data => {
      setServices(data.data.listServices.items);
      let sMap = {};
      data.data.listServices.items.forEach(s => {
        sMap[s.id] = s;
      });
      setServicesMap(sMap);
    });
  }, []);

  const addItem = _ => {
    itemsDispatch({
      type: "addItem",
      data: { itemServiceId: service.id, quantity: quantity }
    });
    setService("");
    setQuantity(1);
  };

  return (
    <table className="table table-borderless">
      <thead>
        <tr>
          <th scope="col">{t("services")}</th>
          <th scope="col">{t("price")}</th>
          <th scope="col">{t("quantity")}</th>
          <th scope="col">{t("total")}</th>
          <th scope="col">{t("action")}</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <select
              className="custom-select d-block w-100"
              id="serviceId"
              value={service ? service.id : ""}
              onChange={e =>
                setService(
                  e.target.value ? servicesMap[e.target.value] : e.target.value
                )
              }
            >
              <option value="">Choose...</option>
              {services.map(s => {
                return (
                  <option key={s.id} value={s.id}>
                    {s.description}: ${s.price}
                  </option>
                );
              })}
            </select>
            <div className="invalid-feedback">{t("invalid.service")}</div>
          </td>
          <td>{service && "$" + service.price}</td>
          <td>
            <select
              className="custom-select d-block w-100"
              id="quantity"
              value={quantity}
              onChange={e => setQuantity(e.target.value)}
            >
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </td>
          <td>{service && "$" + service.price * quantity}</td>
          <td>
            <button
              className="btn btn-secondary"
              type="button"
              disabled={service === ""}
              onClick={addItem}
            >
              +
            </button>
          </td>
        </tr>
        {items.map(item => {
          const service = servicesMap[item.itemServiceId];

          return (
            <tr key={service.id}>
              <td>{service.description}</td>
              <td>${service.price}</td>
              <td>{item.quantity}</td>
              <td>${service.price * item.quantity}</td>
              <td>
                <button
                  className="btn btn-secondary"
                  type="button"
                  onClick={_ =>
                    itemsDispatch({
                      type: "removeItem",
                      data: { itemServiceId: service.id }
                    })
                  }
                >
                  x
                </button>
              </td>
            </tr>
          );
        })}
        <tr>
          <th scope="row">{t("orderTotal")}</th>
          <td></td>
          <td></td>
          <td>
            {"$" +
              items.reduce((total, item) => {
                return (
                  total + item.quantity * servicesMap[item.itemServiceId].price
                );
              }, 0)}
          </td>
          <td></td>
        </tr>
      </tbody>
    </table>
  );
};

export default withTranslation()(Services);
