import React from "react";
import { withTranslation } from "react-i18next";

const Cart = props => {
  const { t } = props;
  return (
    <div className="col-md-4 order-md-2 mb-4">
      <h4 className="d-flex justify-content-between align-items-center mb-3">
        <span className="text-muted">{t("cart.name")}</span>
        <span className="badge badge-secondary badge-pill">3</span>
      </h4>
      <ul className="list-group mb-3">
        <li className="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 className="my-0">{t("cart.products.product1")}</h6>
            <small className="text-muted">
              {t("cart.products.product1_description")}
            </small>
          </div>
          <span className="text-muted">$12</span>
        </li>
        <li className="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 className="my-0">{t("cart.products.product2")}</h6>
            <small className="text-muted">
              {t("cart.products.product2_description")}
            </small>
          </div>
          <span className="text-muted">$8</span>
        </li>
        <li className="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 className="my-0">{t("cart.products.product3")}</h6>
            <small className="text-muted">
              {t("cart.products.product3_description")}
            </small>
          </div>
          <span className="text-muted">$5</span>
        </li>
        <li className="list-group-item d-flex justify-content-between bg-light">
          <div className="text-success">
            <h6 className="my-0">{t("cart.promocode")}</h6>
            <small>EXAMPLECODE</small>
          </div>
          <span className="text-success">-$5</span>
        </li>
        <li className="list-group-item d-flex justify-content-between">
          <span>{t("cart.total")}</span>
          <strong>$20</strong>
        </li>
      </ul>

      <form className="card p-2">
        <div className="input-group">
          <input
            type="text"
            className="form-control"
            maxLength="20"
            placeholder={t("cart.promocode")}
          />
          <div className="input-group-append">
            <button type="submit" className="btn btn-secondary">
              {t("cart.redeem")}
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export const ShoppingCart = withTranslation("cart")(Cart);
