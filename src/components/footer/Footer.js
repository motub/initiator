import React from "react";
import { withTranslation } from "react-i18next";

const Footer = props => {
  const { t } = props;

  return (
    <footer className="my-5 pt-5 text-muted text-center text-small">
      <p className="mb-1">{t("footer.company")}</p>
      <ul className="list-inline">
        <li className="list-inline-item">
          <a href="#top">{t("footer.privacy")}</a>
        </li>
        <li className="list-inline-item">
          <a href="#top">{t("footer.terms")}</a>
        </li>
        <li className="list-inline-item">
          <a href="#top">{t("footer.support")}</a>
        </li>
      </ul>
    </footer>
  );
};

export default withTranslation("footer")(Footer);
