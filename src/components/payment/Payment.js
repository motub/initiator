import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class Payment extends Component {
  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <h4 className="mb-3">{t("payment.title")}</h4>

        <div className="d-block my-3">
          <div className="custom-control custom-radio">
            <input
              id="credit"
              name="paymentMethod"
              type="radio"
              className="custom-control-input"
              defaultValue={true}
              required
            />
            <label className="custom-control-label" htmlFor="credit">
              {t("payment.method.creditcard")}
            </label>
          </div>
          <div className="custom-control custom-radio">
            <input
              id="paypal"
              name="paymentMethod"
              type="radio"
              className="custom-control-input"
              required
            />
            <label className="custom-control-label" htmlFor="paypal">
              {t("payment.method.paypal")}
            </label>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 mb-3">
            <label htmlFor="cc-name">{t("payment.card.details.name")}</label>
            <input
              type="text"
              className="form-control"
              id="cc-name"
              placeholder=""
              required
            />
            <small className="text-muted">
              {t("payment.card.details.help.name")}
            </small>
            <div className="invalid-feedback">
              {t("payment.invalid.card.details.name")}
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <label htmlFor="cc-number">
              {t("payment.card.details.number")}
            </label>
            <input
              type="text"
              className="form-control"
              id="cc-number"
              placeholder=""
              required
            />
            <div className="invalid-feedback">
              {t("payment.invalid.card.details.number")}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 mb-3">
            <label htmlFor="cc-expiration">
              {t("payment.card.details.expiry")}
            </label>
            <input
              type="text"
              className="form-control"
              id="cc-expiration"
              placeholder=""
              required
            />
            <div className="invalid-feedback">
              {t("payment.invalid.card.details.expiry")}
            </div>
          </div>
          <div className="col-md-3 mb-3">
            <label htmlFor="cc-cvv">{t("payment.card.details.seccode")}</label>
            <input
              type="text"
              className="form-control"
              id="cc-cvv"
              placeholder=""
              required
            />
            <div className="invalid-feedback">
              {t("payment.invalid.card.details.seccode")}
            </div>
          </div>
        </div>
        <hr className="mb-4" />
        <button className="btn btn-primary btn-lg btn-block" type="submit">
          {t("payment.checkout.proceed")}
        </button>
      </React.Fragment>
    );
  }
}

export default withTranslation("pay")(Payment);
