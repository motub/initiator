import * as React from "react";
import Footer from "../footer/Footer";
import Details from "./client/Details";
import { ShoppingCart } from "../cart/Cart";

const Job = props => {
  return (
    <div className="container">
      <div className="row">
        <ShoppingCart />
        <Details />
      </div>
      <Footer />
    </div>
  );
};

export default Job;
