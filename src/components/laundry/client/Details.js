import React, { useState, useContext } from "react";
import { withTranslation } from "react-i18next";
import DateTimePicker from "react-datetime-picker";
import { API, graphqlOperation } from "aws-amplify";
import { createJob, createItem } from "../../../graphql/mutations.js";
import StateContext from "../../../contexts/stateContext";
import Services from "../../services/Services";

const Details = ({ t }) => {
  const [isValid, setValid] = useState(true);
  const [isInvalidDelDate, setInvalidDelDate] = useState("d-none");
  const { job, jobDispatch, items } = useContext(StateContext);

  const onSubmit = e => {
    e.preventDefault();
    let validRequiredFields = e.target.checkValidity();

    if (!validRequiredFields) {
      setValid(validRequiredFields);
    }

    let validDates = true;
    if (job.deliveryDate.getDay() <= job.collectionDate.getDay()) {
      setInvalidDelDate("");
      validDates = false;
    } else {
      setInvalidDelDate("d-none");
    }
    if (validRequiredFields && validDates) {
      // AppSync requires an attribute to be set with a non empty
      // string, or not set at all.
      if (!job.address.line2) delete job.address.line2;

      API.graphql(graphqlOperation(createJob, { input: job }))
        .then(result => {
          const jobId = result.data.createJob.id;
          console.log("jobId: " + jobId);
          return Promise.all(
            items.map(i => {
              i.jobCartId = jobId;
              return API.graphql(graphqlOperation(createItem, { input: i }));
            })
          );
        })
        .then(result => {
          alert(JSON.stringify(result, null, 2));
        })
        .catch(error => alert(JSON.stringify(error)));
    }
  };

  return (
    <div className="col-md-8 order-md-1">
      <h4 className="mb-3">Collection address</h4>
      <form
        className={isValid ? "needs-validation" : "was-validated"}
        noValidate
        onSubmit={onSubmit}
      >
        <div className="row">
          <div className="col-md-6 mb-3">
            <label htmlFor="firstName">{t("person.first.name")}</label>
            <input
              type="text"
              className="form-control"
              id="firstName"
              required
              placeholder=""
              value={job.customer.firstname}
              maxLength="100"
              onChange={e =>
                jobDispatch({ type: "firstName", data: e.target.value })
              }
            />
            <div className="invalid-feedback">
              {t("invalid.person.first.name")}
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <label htmlFor="lastName">{t("person.last.name")}</label>
            <input
              type="text"
              className="form-control"
              id="lastName"
              required
              placeholder=""
              value={job.customer.lastname}
              maxLength="100"
              onChange={e =>
                jobDispatch({ type: "lastName", data: e.target.value })
              }
            />
            <div className="invalid-feedback">
              {t("invalid.person.last.name")}
            </div>
          </div>
        </div>

        <div className="mb-3">
          <label htmlFor="email">
            {t("contact.email")}{" "}
            <span className="text-muted">({t("optional")})</span>
          </label>
          <input
            type="email"
            className="form-control"
            id="email"
            required
            maxLength="100"
            value={job.customer.email}
            placeholder={t("contact.placeholder.email")}
            onChange={e => jobDispatch({ type: "email", data: e.target.value })}
          />
          <div className="invalid-feedback">{t("invalid.email")}</div>
        </div>

        <div className="mb-3">
          <label htmlFor="contactNumber">{t("contact.phone")}</label>
          <input
            type="tel"
            className="form-control"
            id="contactNumber"
            required
            maxLength="30"
            value={job.customer.phone}
            onChange={e => jobDispatch({ type: "phone", data: e.target.value })}
            placeholder={t("contact.placeholder.phone")}
          />
          <div className="invalid-feedback">{t("invalid.phone")}</div>
        </div>

        <div className="mb-3">
          <label htmlFor="address">{t("address.line1")}</label>
          <input
            type="text"
            className="form-control"
            id="address1"
            required
            placeholder={t("address.placeholder.line1")}
            maxLength="200"
            value={job.address.line1}
            onChange={e =>
              jobDispatch({ type: "addressLine1", data: e.target.value })
            }
          />
          <div className="invalid-feedback">{t("invalid.address.line1")}</div>
        </div>

        <div className="mb-3">
          <label htmlFor="address2">
            {t("address.line2")}
            <span className="text-muted">({t("optional")})</span>
          </label>
          <input
            type="text"
            className="form-control"
            id="address2"
            maxLength="200"
            value={job.address.line2}
            onChange={e =>
              jobDispatch({ type: "addressLine2", data: e.target.value })
            }
            placeholder={t("address.placeholder.line2")}
          />
        </div>

        <div className="row">
          <div className="col-md-5 mb-3">
            <label htmlFor="suburb">{t("address.suburb")}</label>
            <select
              className="custom-select d-block w-100"
              id="suburb"
              required
              value={job.address.suburb}
              onChange={e =>
                jobDispatch({ type: "addressSuburb", data: e.target.value })
              }
            >
              <option value="">Choose...</option>
              <option>East Brisbane</option>
            </select>
            <div className="invalid-feedback">
              {t("invalid.address.suburb")}
            </div>
          </div>
          <div className="col-md-4 mb-3">
            <label htmlFor="state">{t("address.state")}</label>
            <select
              className="custom-select d-block w-100"
              id="state"
              required
              value={job.address.state}
              onChange={e =>
                jobDispatch({ type: "addressState", data: e.target.value })
              }
            >
              <option value="">Choose...</option>
              <option>California</option>
            </select>
            <div className="invalid-feedback">{t("invalid.address.state")}</div>
          </div>
          <div className="col-md-3 mb-3">
            <label htmlFor="postcode">{t("address.postcode")}</label>
            <input
              type="text"
              className="form-control"
              id="postcode"
              required
              placeholder={t("address.placeholder.postcode")}
              maxLength="10"
              value={job.address.postcode}
              onChange={e =>
                jobDispatch({ type: "addressPostcode", data: e.target.value })
              }
            />
            <div className="invalid-feedback">
              {t("invalid.address.postcode")}
            </div>
          </div>
        </div>

        <hr className="mb-4" />

        <Services />

        <hr className="mb-4" />

        <div className="mb-3">
          <label htmlFor="notes">{t("notes")}s</label>
          <textarea
            className="form-control"
            id="notes"
            placeholder={t("placeholder.notes")}
            maxLength="300"
            value={job.specialNotes}
            onChange={e =>
              jobDispatch({ type: "specialNotes", data: e.target.value })
            }
          />
        </div>

        <div className="row">
          <div className="col-md-6 mb-3">
            <label htmlFor="collectDate">{t("date.collection")}</label>
            <DateTimePicker
              onChange={e => jobDispatch({ type: "collectDate", data: e })}
              minDate={new Date()}
              value={job.collectionDate}
            />
            <div className="invalid-feedback">t{"invalid.date.collection"}</div>
          </div>
          <div className="col-md-6 mb-3">
            <label htmlFor="deliveryDate">{t("date.delivery")}</label>
            <DateTimePicker
              onChange={e => jobDispatch({ type: "deliveryDate", data: e })}
              minDate={new Date()}
              value={job.deliveryDate}
            />
          </div>
        </div>

        <div className={isInvalidDelDate}>
          <div className="mb-3">
            <div className="alert alert-danger" role="alert">
              {t("invalid.date.delivery")}
            </div>
          </div>
        </div>

        <hr className="mb-4" />

        <div className="mb-3">
          <label htmlFor="terms">{t("terms.service")}</label>
          <textarea
            className="form-control"
            id="terms"
            readOnly
            value="Terms are good!"
          />
        </div>
        <div className="custom-control custom-checkbox">
          <input
            type="checkbox"
            className="custom-control-input"
            required
            id="agreeTerms"
          />
          <label className="custom-control-label" htmlFor="agreeTerms">
            {t("terms.agree")}
          </label>
        </div>
        <hr className="mb-4" />
        <button className="btn btn-primary btn-lg btn-block" type="submit">
          {t("pay:payment.checkout.proceed")}
        </button>
      </form>
    </div>
  );
};

export default withTranslation()(Details);
