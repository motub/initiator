/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateService = /* GraphQL */ `
  subscription OnCreateService {
    onCreateService {
      id
      description
      price
    }
  }
`;
export const onUpdateService = /* GraphQL */ `
  subscription OnUpdateService {
    onUpdateService {
      id
      description
      price
    }
  }
`;
export const onDeleteService = /* GraphQL */ `
  subscription OnDeleteService {
    onDeleteService {
      id
      description
      price
    }
  }
`;
export const onCreateItem = /* GraphQL */ `
  subscription OnCreateItem {
    onCreateItem {
      id
      service {
        id
        description
        price
      }
      quantity
    }
  }
`;
export const onUpdateItem = /* GraphQL */ `
  subscription OnUpdateItem {
    onUpdateItem {
      id
      service {
        id
        description
        price
      }
      quantity
    }
  }
`;
export const onDeleteItem = /* GraphQL */ `
  subscription OnDeleteItem {
    onDeleteItem {
      id
      service {
        id
        description
        price
      }
      quantity
    }
  }
`;
export const onCreateJob = /* GraphQL */ `
  subscription OnCreateJob {
    onCreateJob {
      id
      customer {
        firstname
        lastname
        email
        phone
      }
      address {
        line1
        line2
        suburb
        state
        postcode
      }
      cart {
        items {
          id
          quantity
        }
        nextToken
      }
      specialNotes
      collectionDate
      deliveryDate
    }
  }
`;
export const onUpdateJob = /* GraphQL */ `
  subscription OnUpdateJob {
    onUpdateJob {
      id
      customer {
        firstname
        lastname
        email
        phone
      }
      address {
        line1
        line2
        suburb
        state
        postcode
      }
      cart {
        items {
          id
          quantity
        }
        nextToken
      }
      specialNotes
      collectionDate
      deliveryDate
    }
  }
`;
export const onDeleteJob = /* GraphQL */ `
  subscription OnDeleteJob {
    onDeleteJob {
      id
      customer {
        firstname
        lastname
        email
        phone
      }
      address {
        line1
        line2
        suburb
        state
        postcode
      }
      cart {
        items {
          id
          quantity
        }
        nextToken
      }
      specialNotes
      collectionDate
      deliveryDate
    }
  }
`;
