/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getService = /* GraphQL */ `
  query GetService($id: ID!) {
    getService(id: $id) {
      id
      description
      price
    }
  }
`;
export const listServices = /* GraphQL */ `
  query ListServices(
    $filter: ModelServiceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listServices(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        description
        price
      }
      nextToken
    }
  }
`;
export const getItem = /* GraphQL */ `
  query GetItem($id: ID!) {
    getItem(id: $id) {
      id
      service {
        id
        description
        price
      }
      quantity
    }
  }
`;
export const listItems = /* GraphQL */ `
  query ListItems(
    $filter: ModelItemFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listItems(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        service {
          id
          description
          price
        }
        quantity
      }
      nextToken
    }
  }
`;
export const getJob = /* GraphQL */ `
  query GetJob($id: ID!) {
    getJob(id: $id) {
      id
      customer {
        firstname
        lastname
        email
        phone
      }
      address {
        line1
        line2
        suburb
        state
        postcode
      }
      cart {
        items {
          id
          quantity
        }
        nextToken
      }
      specialNotes
      collectionDate
      deliveryDate
    }
  }
`;
export const listJobs = /* GraphQL */ `
  query ListJobs(
    $filter: ModelJobFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listJobs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        customer {
          firstname
          lastname
          email
          phone
        }
        address {
          line1
          line2
          suburb
          state
          postcode
        }
        cart {
          nextToken
        }
        specialNotes
        collectionDate
        deliveryDate
      }
      nextToken
    }
  }
`;
