import React, { Suspense } from "react";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useReducer } from "react";
import Job from "./components/laundry/Job";
import Confirmation from "./components/laundry/Confirmation";
import Amplify from "aws-amplify";
import awsconfig from "./aws-exports";
import StateContext from "./contexts/stateContext";
import { jobReducer, initialState } from "./reducers/jobReducer";
import { itemsReducer } from "./reducers/itemsReducer";

Amplify.configure(awsconfig);
const Loader = () => (
  <div className="App">
    <div>loading...</div>
  </div>
);

const App = _ => {
  const [job, jobDispatch] = useReducer(jobReducer, initialState);
  const [items, itemsDispatch] = useReducer(itemsReducer, []);

  return (
    <StateContext.Provider value={{ job, jobDispatch, items, itemsDispatch }}>
      <Suspense fallback={<Loader />}>
        <BrowserRouter>
          <main>
            <Switch>
              <Route path="/" component={Job} exact />
              <Route path="/confirmation" component={Confirmation} exact />
            </Switch>
          </main>
        </BrowserRouter>
      </Suspense>
    </StateContext.Provider>
  );
};

export default App;
